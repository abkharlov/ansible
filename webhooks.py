from http.server import BaseHTTPRequestHandler, HTTPServer
import json

LOG_FILE = "webhook_logs.json"

class WebhookHandler(BaseHTTPRequestHandler):
    def do_POST(self):
        content_length = int(self.headers['Content-Length'])
        post_data = self.rfile.read(content_length).decode('utf-8')

        print("\nReceived Webhook:\n", post_data)

        # Запись в файл
        try:
            with open(LOG_FILE, "a") as f:
                f.write(post_data + "\n")
        except Exception as e:
            print("Error writing to file:", e)

        # Ответ вебхуку
        self.send_response(200)
        self.send_header('Content-Type', 'application/json')
        self.end_headers()
        self.wfile.write(json.dumps({"status": "received"}).encode())

server_address = ('', 3333)
httpd = HTTPServer(server_address, WebhookHandler)
print(f"Listening on port {server_address[1]}... Webhooks will be logged to {LOG_FILE}")
httpd.serve_forever()